import Data.List

-- Sort a list of lists according to length of sublists.
lsort :: [[a]] -> [[a]]
lsort [] = []
lsort (x:xs) = lsort [m | m <- xs, length m <= length x] 
               ++ [x] ++
               lsort [m | m <- xs, length m > length x] 

lfsort :: [[a]] -> [[a]]
-- lfsort xs = let freqs = map head $ group $ sort $ map length xs
--             in map (\f -> filter (\x -> length x < f) xs) freqs
lfsort xs = concat gs
  where gs = lsort $ groupBy ll $ lsort xs
        ll ys zs = length ys == length zs

main = do
    print $ lsort ["abc","de","fgh","de","ijkl","mn","o"]
    print $ lfsort ["fabc","de","fgh","de","ijkl","mn","o"]
