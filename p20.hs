-- Remove the K'th element from a list.

removeAt :: Int -> [a] -> (a, [a])
removeAt 0 (x:xs) = (x,xs)
removeAt n (x:xs) = let (f, s) = removeAt (n-1) xs
                    in (f, x:s)

main = do
    print $ removeAt 2 "abcdefg"
