-- Given a range of integers by its lower and upper limit, print a list of
-- all even numbers and their Goldbach composition.
-- Also, find pairs that have both elements greater than some limit.

is_prime :: Int -> Bool
is_prime x = not $ True `elem` [x `mod` y == 0 | y <- [2..x-1]]

primesR :: Int -> Int -> [Int]
primesR m n = filter is_prime [m..n]

goldbach :: Int -> (Int, Int)
goldbach n = head [(x, y) | x <- primes, y <- primes, x + y == n]
    where primes = primesR 2 n

goldBachList :: Int -> Int -> [(Int, Int)]
goldBachList m n = map goldbach evens
    where evens = filter even [m..n]

goldBachList' :: Int -> Int -> Int -> [(Int, Int)]
goldBachList' m n l = filter (\x -> (fst x > l) && (snd x > l)) $ map goldbach evens
    where evens = filter even [m..n]

main = do
    print $ goldBachList' 4 1000 50
