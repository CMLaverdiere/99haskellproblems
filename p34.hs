-- Calculate Euler's totient function phi(m)
-- ie: the number of integers r (1 <= r < m) that are coprime to m.

coprime :: Int -> Int -> Bool
coprime m n = 1 == gcd m n

totient :: Int -> Int
totient m = length $ filter (coprime m) [1..m-1]

main = do
    print $ totient 10
