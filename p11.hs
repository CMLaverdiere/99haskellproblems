-- Modified run length encoding.
 
data ListItem a = Single a | Multiple Int a deriving (Show)

pack :: (Eq a) => [a] -> [[a]]
pack [] = []
pack (x:xs) = (x:m) : pack nm
    where (m,nm) = span (== x) xs
   
encode :: (Eq a) => [a] -> [(Int, a)]
encode xs = zip (map length grouped) singles
    where grouped = pack xs
          singles = map head grouped
 
encode_mod :: (Eq a) => [a] -> [ListItem a]
encode_mod = map cardinality_mod . encode
  where cardinality_mod (1, a) = Single a
        cardinality_mod (x, a) = Multiple x a

main = do
    print $ encode_mod "aaaabccaadeeee"
