-- Rotate a list N places to the right.

rotate :: [a] -> Int -> [a]
rotate xs n 
  | n > 0     = drop (len-n) xs ++ take (len-n) xs
  | otherwise = drop (-n) xs ++ take (-n) xs
    where
      len = length xs

main = do
    print $ rotate [1,2,3,4,5] 1
