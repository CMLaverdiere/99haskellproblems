-- Replicate the elements of a list a given number of times.

repli :: [a] -> Int -> [a]
repli [] _ = []
repli [x] 1 = [x]
repli (x:xs) n = x : (repli [x] (n-1)) ++ (repli xs n)

main = do
    print $ repli "fuck pots" 8
