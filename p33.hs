-- Determine whether two integers are coprime.
-- ie: their GCD = 1.

coprime :: Int -> Int -> Bool
coprime m n = 1 == gcd m n

main = do
    print $ coprime 35 64
