-- Eliminate consecutive duplicates of list elements.
compress :: (Eq a) => [a] -> [a]
compress [x] = [x]
compress (x:xs)
    | head xs == x = compress xs
    | otherwise    = [x] ++ compress xs
 
main = do
    print $ compress "aaaabccaadeeee"
