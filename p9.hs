-- Pack consecutive list elements into sublists.
 
pack :: (Eq a) => [a] -> [[a]]
pack [] = []
pack (x:xs) = (x:m) : pack nm
    where (m,nm) = span (== x) xs
 
main = do
    print $ pack [1,1,1,2,2,3,4,4,4,4,5,5]
 
