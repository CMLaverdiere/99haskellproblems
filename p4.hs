-- Find the number of elements in a list.
myLength :: [a] -> Int
myLength [] = 0
myLength [x] = 1
myLength (x:xs) = 1 + myLength xs

main = do
    print $ myLength ["hi", "there", "matey"]
