-- Find the last but one element of a list.
myButLast :: [a] -> a
myButLast [x,y] = x
myButLast (x:xs) = myButLast xs

main = do
    print $ myButLast [1..20]
