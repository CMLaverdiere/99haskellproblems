-- Split a list into two parts, given the first length.

split :: [a] -> Int -> ([a], [a])
split (x:xs) 0 = ([x], xs)
split (x:xs) n = (x : fst rest, snd rest)
  where
    rest = split xs (n-1)

main = do
    print $ split "abcdefghik" 3
