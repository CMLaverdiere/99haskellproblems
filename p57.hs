-- Construct binary trees.

data Tree a = Empty | Branch a (Tree a) (Tree a) 
              deriving (Show, Eq)

leaf :: a -> Tree a
leaf a = Branch a Empty Empty

insert :: (Ord a) => a -> Tree a -> Tree a
insert x Empty = leaf x
insert x (Branch y l r)
  | x < y = Branch y (insert x l) r
  | x > y = Branch y l (insert x r)
  | x == y = (Branch x l r)

construct :: (Ord a) => [a] -> Tree a
construct xs = foldl (flip insert) Empty xs

main = do
  print $ construct [3,2,5,7,1]
