-- group elements of a set into disjoint subsets.

import Data.List

deleteAll :: (Eq a) => [a] -> [a] -> [a]
deleteAll ds xs = foldr delete xs ds

combinations :: Int -> [a] -> [[a]]
combinations 0 _ = [[]]
combinations n xs = [xs !! i : x | i <- [0..(length xs)-1]
                                 , x <- combinations (n-1) (drop (i+1) xs)]

-- For each combinatoral n-tuple, recurse with those items
-- removed.
grp :: (Eq a) => [Int] -> [a] -> [[[a]]]
grp [] _ = [[]]
grp (n:ns) xs = [[g] ++ rest | g <- gs, 
                               rest <- grp ns (deleteAll g xs)]
    where
      gs = combinations n xs

main = do
    print $ length $ grp [2,2,5] [1,2,3,5,8,13,21,34,55]
