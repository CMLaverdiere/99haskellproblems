-- Generate a truth table of n variables for any expression.

import Control.Monad

not' :: Bool -> Bool
not' False = True
not' True = False

and' :: Bool -> Bool -> Bool
and' True True = True
and' _ _ = False

or' :: Bool -> Bool -> Bool
or' True _ = True
or' _ _ = False

nand' :: Bool -> Bool -> Bool
nand' a b = not' $ and' a b

nor' :: Bool -> Bool -> Bool
nor' a b = not' $ or' a b

xor' :: Bool -> Bool -> Bool
xor' True False = True
xor' False True = True
xor' _ _ = False

impl' :: Bool -> Bool -> Bool
impl' True False = False
impl' _ _ = True

tablen :: Int -> ([Bool] -> Bool) -> [[Bool]]
tablen n f = [x ++ [f x] | x <- replicateM n [True, False]]

printtableN :: [[Bool]] -> String
printtableN t = unlines $ map show t

main = do
    putStr . printtableN $ tablen 4 (\[w,x,y,z] -> (w `or'` x `or'` (y `and'` z)))
