-- Generate the combinations of K distinct objects chosen from the
-- N elements of a list.

combinations :: Int -> [a] -> [[a]]
combinations 0 _ = [[]]
combinations n xs = [xs !! i : x | i <- [0..(length xs)-1]
                                 , x <- combinations (n-1) (drop (i+1) xs)]

main = do
    print $ combinations 3 "abcde"
