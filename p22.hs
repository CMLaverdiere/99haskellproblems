-- Create a list containing all integers within a given range.

range :: Int -> Int -> [Int]
range l u = take (u-l+1) $ map (+l) [0..]

main = do
    print $ range 4 9
