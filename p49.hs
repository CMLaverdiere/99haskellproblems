-- Generate an n-bit gray code.
gray :: Int -> [String]
gray 0 = [""]
gray n = map ('0':) old ++ map ('1':) (reverse old)
  where old = gray (n-1)

main = do
    print $ gray 5
