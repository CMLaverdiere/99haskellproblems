-- Symmetric binary trees.

data Tree a = Empty | Branch a (Tree a) (Tree a) 
              deriving (Show, Eq)

leaf :: a -> Tree a
leaf a = Branch a Empty Empty

-- Check if one tree is a mirror image of another.
mirror :: Tree a -> Tree a -> Bool
mirror Empty Empty = True
mirror (Branch x1 l1 r1) (Branch x2 l2 r2) = (mirror l1 l2) 
                                          && (mirror r1 r2)
mirror _ _ = False

symmetric :: Tree a -> Bool
symmetric (Branch x l r) = mirror l r

main = do
  print $ symmetric (Branch 'x' (Branch 'x' Empty Empty) Empty)
