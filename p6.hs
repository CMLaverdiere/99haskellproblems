-- Find out whether a list is a palindrome.
-- Got stuck on this one. Watch for (x:xs) vs xs.
isPalindrome :: (Eq a) => [a] -> Bool
isPalindrome []  = True
isPalindrome [_] = True
isPalindrome xs  = (head xs) == (last xs) && (isPalindrome $ init $ tail xs)

main = do
    print $ isPalindrome [2,1,3,1,2]
