-- Determine prime factors of a given positive integer.

primeFactors :: Int -> [Int]
-- primeFactors n = filter (\x -> is_prime x  && n `mod` x == 0) [2..n]
primeFactors n = primeFactors' n 2
  where
    primeFactors' n x
      | n < 2 = []
      | n `mod` x == 0 = x : primeFactors' (n `div` x) x
      | otherwise      = primeFactors' n (x+1)

main = do
    print $ primeFactors (13*92*57*24*96*23*42)
