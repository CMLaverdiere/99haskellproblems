-- Construct completely balanced binary trees.
-- Produce all permutations of balanced tree.

data Tree a = Empty | Branch a (Tree a) (Tree a) 
              deriving (Show, Eq)

leaf :: a -> Tree a
leaf a = Branch a Empty Empty

cbalTree :: Int -> [Tree Char]
cbalTree 0 = [Empty]
cbalTree n = let (q, r) = (n-1) `quotRem` 2
  in [Branch 'x' left right | i     <- [q .. q+r],
                              left  <- cbalTree i,
                              right <- cbalTree (n - 1 - i)]

main = do
  putStr $ unlines . map (\x -> "{ " ++ show x ++ " }") $ cbalTree 4
