-- Determine prime factors of a given positive integer.

import Data.List

primeFactors :: Int -> [Int]
-- primeFactors n = filter (\x -> is_prime x  && n `mod` x == 0) [2..n]
primeFactors n = primeFactors' n 2
  where
    primeFactors' n x
      | n < 2 = []
      | n `mod` x == 0 = x : primeFactors' (n `div` x) x
      | otherwise      = primeFactors' n (x+1)

primeFactorsMult :: Int -> [(Int, Int)]
primeFactorsMult n = let factors = primeFactors n
                     in map (\x -> (head x, length x)) $ group factors

main = do
    print $ primeFactorsMult 315
