-- Reverse a list
myReverse :: [a] -> [a]
myReverse [x] = [x]
myReverse (x:xs) = myReverse xs ++ [x]

main = do
    print $ myReverse [1, 6, 5, 9]

