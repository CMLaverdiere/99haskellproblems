import Data.List

primeFactors :: Int -> [Int]
primeFactors n = primeFactors' n 2
  where
    primeFactors' n x
      | n < 2 = []
      | n `mod` x == 0 = x : primeFactors' (n `div` x) x
      | otherwise      = primeFactors' n (x+1)

primeFactorsMult :: Int -> [(Int, Int)]
primeFactorsMult n = let factors = primeFactors n
                     in map (\x -> (head x, length x)) $ group factors

phi :: [(Int, Int)] -> Int
phi ps = foldr (*) 1 $ map (\p -> (fst p - 1) * (fst p ^ (snd p - 1))) ps

main = do
    print $ phi $ primeFactorsMult 10
