-- Decode a run-length encoded list.
 
data ListItem a = Single a | Multiple Int a deriving (Show)

decodeModified :: [ListItem a] -> [a]
decodeModified xs = concatMap helper xs
  where helper (Multiple x a) = replicate x a
        helper (Single a) = [a]

main = do
    print $ decodeModified [Multiple 4 'a',Single 'b',Multiple 2 'c',
                            Multiple 2 'a',Single 'd',Multiple 4 'e']
