-- Huffman codes.
-- This is so ugly!

-- requires package priority-queue
-- import Data.PriorityQueue

import Data.List
import Data.Ord (comparing)

data HTree s f = HLeaf s f | HNode f (HTree s f) (HTree s f) 
               deriving (Show)


sumProbs :: (HTree Char Int) -> (HTree Char Int) -> Int
sumProbs (HNode f1 _ _) (HNode f2 _ _) = f1 + f2
sumProbs (HNode f1 _ _) (HLeaf _ f2) = f1 + f2
sumProbs (HLeaf _ f2) (HNode f1 _ _) = f1 + f2
sumProbs (HLeaf _ f1) (HLeaf _ f2) = f1 + f2


sortHTree :: (HTree Char Int) -> (HTree Char Int) -> Ordering
sortHTree (HNode f1 _ _) (HNode f2 _ _) = f1 `compare` f2
sortHTree (HNode f1 _ _) (HLeaf _ f2) = f1 `compare` f2
sortHTree (HLeaf _ f2) (HNode f1 _ _) = f1 `compare` f2
sortHTree (HLeaf _ f1) (HLeaf _ f2) = f1 `compare` f2

tracePath :: (HTree Char Int) -> [(Char, String)]
tracePath (HLeaf s _) = [(s, "")]
tracePath (HNode _ t1 t2) = (map (\(x,y) -> (x, "0" ++ y)) $ tracePath t1) 
                         ++ (map (\(x,y) -> (x, "1" ++ y)) $ tracePath t2)

huffman :: [(Char, Int)] -> [(Char, String)]
huffman items = let pqueue = sortBy (comparing snd) items
                    leaves = [HLeaf (fst x) (snd x) | x <- pqueue]
                    htree = construct leaves
                in sortBy (comparing fst) $ tracePath htree


construct :: [(HTree Char Int)] -> (HTree Char Int)
construct items 
  | length items == 1 = head items
  | otherwise         = let t1 = (items !! 0)
                            t2 = (items !! 1)
                            newnode = HNode (sumProbs t1 t2) t1 t2
                        in construct . sortBy sortHTree $ newnode : drop 2 items 


main = do
    print $ huffman [('a',45),('b',13),('c',12),('d',16),('e',9),('f',5)]
