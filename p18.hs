-- Extract a slice from a list.

slice :: [a] -> Int -> Int -> [a]
slice [] _ _ = []
slice (x:xs) 0 0 = [x]
slice (x:xs) 0 n = x : slice xs 0 (n-1)
slice (x:xs) m n = slice xs (m-1) (n-1)

main = do
    print $ slice "classy" 2 4
