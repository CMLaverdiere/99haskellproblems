-- Find the Kth element of a list, with first element having index 1.
elementAt :: [b] -> Int -> b 
elementAt (x:xs) 1 = x
elementAt (x:xs) n = elementAt xs $ n-1 

main = do
    -- I'm bored.
    print $ elementAt [x * 9 | x <- [1..]] 199
