-- Define predicates and, or, nand, nor, xor, impl, and equ
-- Also write a predicate table with prints a truth table.

type Truth3 = (Bool, Bool, Bool)

not' :: Bool -> Bool
not' False = True
not' True = False

and' :: Bool -> Bool -> Bool
and' True True = True
and' _ _ = False

or' :: Bool -> Bool -> Bool
or' True _ = True
or' _ _ = False

nand' :: Bool -> Bool -> Bool
nand' a b = not' $ and' a b

nor' :: Bool -> Bool -> Bool
nor' a b = not' $ or' a b

xor' :: Bool -> Bool -> Bool
xor' True False = True
xor' False True = True
xor' _ _ = False

impl' :: Bool -> Bool -> Bool
impl' True False = False
impl' _ _ = True

table :: (Bool -> Bool -> Bool) -> [Truth3]
table f = map (\x -> (fst x, snd x, f (fst x) (snd x))) perms
    where perms = [(True, True), 
                   (True, False), 
                   (False, True), 
                   (False, False)]

printTable :: [Truth3] -> String
printTable t = unlines [show x ++ " | " ++ show y ++ " | " ++ show z 
                 | (x,y,z) <- t]

main = do
    putStr . printTable $ table (\a b -> (and' a (or' a b)))
