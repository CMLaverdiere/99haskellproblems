-- Construct a list of prime numbers in a given range.

-- Determine whether a given integer number is prime.
is_prime :: Int -> Bool
is_prime x = not $ True `elem` [x `mod` y == 0 | y <- [2..x-1]]

primesR :: Int -> Int -> [Int]
primesR m n = filter is_prime [m..n]

main = do
    print $ primesR 10 20
