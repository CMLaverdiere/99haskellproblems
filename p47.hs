-- Adapt problem 46 to infix functions.

type Truth3 = (Bool, Bool, Bool)

not' :: Bool -> Bool
not' False = True
not' True = False

and' :: Bool -> Bool -> Bool
and' True True = True
and' _ _ = False

or' :: Bool -> Bool -> Bool
or' True _ = True
or' _ _ = False

nand' :: Bool -> Bool -> Bool
nand' a b = not' $ and' a b

nor' :: Bool -> Bool -> Bool
nor' a b = not' $ or' a b

xor' :: Bool -> Bool -> Bool
xor' True False = True
xor' False True = True
xor' _ _ = False

impl' :: Bool -> Bool -> Bool
impl' True False = False
impl' _ _ = True

table2 :: (Bool -> Bool -> Bool) -> [Truth3]
table2 f = map (\x -> (fst x, snd x, f (fst x) (snd x))) perms
    where perms = [(True, True), 
                   (True, False), 
                   (False, True), 
                   (False, False)]

printtable2 :: [Truth3] -> String
printtable2 t = unlines [show x ++ " | " ++ show y ++ " | " ++ show z 
                 | (x,y,z) <- t]

main = do
    putStr . printtable2 $ table2 (\a b -> (a `and'` (a `or'` b)))
