-- Determine greatest common divisor of two positive integers.
-- Euclid's algorithm.
gcd' :: Int -> Int -> Int
gcd' m n 
  | dv * b == a = b
  | otherwise   = gcd' b (a - (dv * b))
  where a = max m n
        b = min m n
        dv = a `div` b

main = do
    print $ gcd' 108 14
