-- Goldbach's conjecture.
-- ie: Every number is the some of two primes.

is_prime :: Int -> Bool
is_prime x = not $ True `elem` [x `mod` y == 0 | y <- [2..x-1]]

primesR :: Int -> Int -> [Int]
primesR m n = filter is_prime [m..n]

goldbach :: Int -> [(Int, Int)]
goldbach n = [(x, y) | x <- primes, y <- primes, x + y == n]
    where primes = primesR 2 n

main = do
    print $ goldbach 28
