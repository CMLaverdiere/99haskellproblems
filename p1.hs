-- Find the Last element of a list.
myLast :: [a] -> a
myLast [x] = x
myLast (x:xs) = myLast xs

main = do
    print $ myLast [1..20]
