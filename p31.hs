-- Determine whether a given integer number is prime.
is_prime :: Int -> Bool
is_prime x = not $ True `elem` [x `mod` y == 0 | y <- [2..x-1]]

main = do
    print $ filter is_prime [1..20] 
