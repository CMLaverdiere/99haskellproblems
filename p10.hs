-- Pack consecutive list elements into sublists.
 
pack :: (Eq a) => [a] -> [[a]]
pack [] = []
pack (x:xs) = (x:m) : pack nm
    where (m,nm) = span (== x) xs
   
encode :: (Eq a) => [a] -> [(Int, a)]
encode xs = zip (map length grouped) singles
    where grouped = pack xs
          singles = map head grouped
 
main = do
    print $ encode "aaaabccaadeeee"
