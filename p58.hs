-- Generate-and-test paradigm

data Tree a = Empty | Branch a (Tree a) (Tree a) 
              deriving (Show, Eq)

leaf :: a -> Tree a
leaf a = Branch a Empty Empty

cbalTree :: Int -> [Tree Char]
cbalTree 0 = [Empty]
cbalTree n = let (q, r) = (n-1) `quotRem` 2
  in [Branch 'x' left right | i     <- [q .. q+r],
                              left  <- cbalTree i,
                              right <- cbalTree (n - 1 - i)]

mirror :: Tree Char -> Tree Char -> Bool
mirror Empty Empty = True
mirror (Branch x1 l1 r1) (Branch x2 l2 r2) = (mirror l1 l2) 
                                          && (mirror r1 r2)
mirror _ _ = False

symmetric :: Tree Char -> Bool
symmetric (Branch x l r) = mirror l r

-- A little bit cheating, but whatever.
symCbalTrees :: Int -> [Tree Char]
symCbalTrees 0 = [Empty]
symCbalTrees x = filter symmetric (cbalTree x)
                 
main = do
  print $ symCbalTrees 5
